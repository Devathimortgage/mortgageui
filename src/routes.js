import React from 'react';

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));

const Customer = React.lazy(() => import('./views/customer/Customer'));

const routes = [
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/customer', name: 'Customer',component: Customer},
  
]

export default routes;
