import React, { useEffect, useState } from 'react'
import IdleTimer from 'react-idle-timer';
import {Redirect} from 'react-router-dom';
import {
  TheCustomerContent,
  TheCustomerSidebar,
  TheCustomerFooter,
  TheCustomerHeader
} from './customerindex'

const TheCustomerLayout = () => {
  const [time,setTime] = useState(10000);
  const [isTimedOut,setIsTimedOut] = useState(false);
  const [isLogged,setLogged] = useState(false);
  const [userName,setUserName] = useState("");
  const handleOnAction = (event) =>{
    console.log('user did something', event)
  }
 useEffect(()=>{
  let userName = sessionStorage.getItem('userName');
  if(userName !==null)
  {
      userName = userName.split("@")[0];
      setUserName(userName);
      setLogged(true);
      let maxTime = sessionStorage.getItem('sessionMaxTime');
      setTime((maxTime/60)*1000);
  }
 })
  const handleOnActive = (event)=> {
    console.log('user is active', event)
    // console.log('time remaining', this.idleTimer.getRemainingTime())
  }
 
  const handleOnIdle = (event)=> {
    console.log('user is idle', event)
    setIsTimedOut(true);
    window.alert("Session expired. Please Login Again")
   
    sessionStorage.clear();
    // console.log('last active', this.idleTimer.getLastActiveTime())
  }
  return (
    <div className="c-app c-default-layout">
       {isLogged?<IdleTimer
            // ref={ref => { this.idleTimer = ref }}
            element={document}
            onActive={handleOnActive}
            onIdle={handleOnIdle}
            onAction={handleOnAction}
            debounce={250}
            timeout={time} />:null}
        {isTimedOut?  <Redirect to="/login"/>:null}
      <TheCustomerSidebar/>
      <div className="c-wrapper">
        <TheCustomerHeader userName={userName}/>
        <div className="c-body">
          <TheCustomerContent/>
        </div>
        <TheCustomerFooter/>
      </div>
    </div>
  )
}

export default TheCustomerLayout
