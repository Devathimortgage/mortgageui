import TheCustomerContent from './TheCustomerContent'
import TheCustomerFooter from './TheCustomerFooter'
import TheCustomerHeader from './TheCustomerHeader'
import TheCustomerHeaderDropdown from './TheCustomerHeaderDropdown'
import TheCustomerHeaderDropdownMssg from './TheCustomerHeaderDropdownMssg'
import TheCustomerHeaderDropdownNotif from './TheCustomerHeaderDropdownNotif'
import TheCustomerHeaderDropdownTasks from './TheCustomerHeaderDropdownTasks'
import TheCustomerLayout from './TheCustomerLayout'
import TheCustomerSidebar from './TheCustomerSidebar'

export {
  TheCustomerContent,
  TheCustomerFooter,
  TheCustomerHeader,
  TheCustomerHeaderDropdown,
  TheCustomerHeaderDropdownMssg,
  TheCustomerHeaderDropdownNotif,
  TheCustomerHeaderDropdownTasks,
  TheCustomerLayout,
  TheCustomerSidebar
}
