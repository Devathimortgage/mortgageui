export default [

  {
    _tag: 'CSidebarNavItem',
    name: 'My Applications',
    to: '/customer',
    icon: 'cilList',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Start New Application',
    to: '/customer',
    icon: 'cilLibraryAdd',
  },
  
]

