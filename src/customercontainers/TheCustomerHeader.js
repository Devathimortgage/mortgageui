import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  CHeader,
  CToggler,
  CHeaderBrand,
  CHeaderNav,
  CHeaderNavItem,
  CHeaderNavLink,
  CSubheader,
  CBreadcrumbRouter,
  CLink,
  CNavbarBrand
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

// routes config
import routes from '../routes'

import { 
  TheCustomerHeaderDropdown,
  TheCustomerHeaderDropdownMssg,
  TheCustomerHeaderDropdownNotif,
  TheCustomerHeaderDropdownTasks
}  from './customerindex'

const TheCustomerHeader = (props) => {
  const dispatch = useDispatch()
  const sidebarShow = useSelector(state => state.sidebarShow)

  const toggleSidebar = () => {
    const val = [true, 'responsive'].includes(sidebarShow) ? false : 'responsive'
    dispatch({type: 'set', sidebarShow: val})
  }

  const toggleSidebarMobile = () => {
    const val = [false, 'responsive'].includes(sidebarShow) ? true : 'responsive'
    dispatch({type: 'set', sidebarShow: val})
  }

  return (
    <CHeader withSubheader>
      <CToggler
        inHeader
        className="ml-md-3 d-lg-none"
        onClick={toggleSidebarMobile}
      />
      <CToggler
        inHeader
        className="ml-3 d-md-down-none"
        onClick={toggleSidebar}
      />
      <CHeaderBrand className="mx-auto d-lg-none" to="/">
      <h6 name="logo" height="48" alt="Logo"> Mortgage Application</h6>
     </CHeaderBrand>

      <CHeaderNav className="d-md-down-none mr-auto">
        <CHeaderNavItem className="px-3" >
          <CHeaderNavLink to="/customer">My Applications</CHeaderNavLink>
        </CHeaderNavItem>
        <CHeaderNavItem  className="px-3">
          <CHeaderNavLink to="/customer">Start New Application</CHeaderNavLink>
        </CHeaderNavItem>
        
      </CHeaderNav>
      
      <CHeaderNav className="px-3">
      <CHeaderNavItem  className="px-3">
          <CHeaderNavLink >Welcome {props.userName}</CHeaderNavLink>
        </CHeaderNavItem>
        <TheCustomerHeaderDropdownNotif/>
        <TheCustomerHeaderDropdown/>
      </CHeaderNav>
      </CHeader>

  
    )
}

export default TheCustomerHeader
