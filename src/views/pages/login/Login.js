import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import  IdleTimer  from 'react-idle-timer'
import axios from 'axios';
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import Navigation from '../../home/Navigation'
import { Redirect } from 'react-router-dom/cjs/react-router-dom.min';
const LogoutModal = ({})=>{
  return(
    <span></span>
  )
}

const Login = () => {
  const [userName,SetUserName] = useState("");
  const [password,SetPassword] = useState("");
  // const [time,setTime] = useState(1000);
  const [isLogged,setIsLogged] = useState(false);
  // const [isTimedOut,setIsTimedOut] = useState(false);
  const handleUserName = (event)=>{
     SetUserName(event.target.value);
  }
  const handlePassword = (event)=>{
    SetPassword(event.target.value);
  }
  const handleLogin = ()=>{
    console.log("Username:",userName);
    // console.log("Password:",password);
    console.log(btoa(userName+":"+password))
    let encodedString = "Basic "+btoa(userName+":"+password)
    let encodedOrg = btoa('Lender')
    console.log(encodedString,encodedOrg);
    axios.post('http://35.232.52.140:8080/authenticate/login',{},
    {headers:{'authorization':encodedString,"organization":encodedOrg}}).then(
      (response)=>{
        console.log("Login succssful",response.data);
        let data = response.data;
        sessionStorage.setItem("jwt",data.jwt);
        sessionStorage.setItem("sessionMaxTime",data.sessionMaxTime);
        sessionStorage.setItem("authorities",data.authorities);
        sessionStorage.setItem("userName",userName);
        setIsLogged(true);
        // setTime(data.sessionMaxTime);
      }
    ).catch((e)=>{
      console.log("Login Failed");
      // setIsLogged(true);
      // sessionStorage.setItem("userName",userName);

    })
    
  }
   
  return (
  
    <div className="c-app c-default-layout flex-row align-items-center"
    style={{background:'rebeccapurple'}}>
      
        
        {isLogged?<Redirect to="/customer"/>:null}
       <Navigation/>
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="6">
              <CCard className="p-4">
                <CCardBody>
                  <CForm>
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user"/>
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput id="login_username" type="text" placeholder="Username" autoComplete="username" onChange={handleUserName}/>
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput id="login_password" type="password" placeholder="Password" autoComplete="current-password" onChange={handlePassword} />
                    </CInputGroup>
                    <CRow>
                      <CCol xs="6">
                        <CButton color="primary" className="px-4" onClick={handleLogin}>Login</CButton>
                      </CCol>
                      <CCol xs="6" className="text-right">
                        <CButton color="link" className="px-0">Forgot password?</CButton>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
           
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Login
