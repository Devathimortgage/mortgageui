import React from 'react';
import FormDetails from './FormDetails';
import Navigation from './Navigation';
import Footer from './Footer';

export default class FormPageLayout extends React.Component {
render(){
    return (
        <>
        <Navigation/>
        <section className="home_banner_area" style={{textAlign: 'center',background:'rebeccapurple'}}>
            <div className="banner_inner">
              <div className="container">
                <div className="row">
                <div className="col">
                    <FormDetails/>
                    </div>
                    </div>
                    </div>
                    </div>
                    </section>
            <Footer/>
        </>
      
    );
}
}