import React from 'react';
import axios from "axios";

export default class FormDetails extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
       currentTab : 0,
       responseData:{userId:'',applicationId:''},
       errorResponse:{firstName:[],lastName:[],username:[],password:[]}
          }
    this.showTab = this.showTab.bind(this);
    this.fixStepIndicator = this.fixStepIndicator.bind(this);
    this.nextPrev = this.nextPrev.bind(this);
    this.validateForm = this.validateForm.bind(this);
    this.formSubmit = this.formSubmit.bind(this);
    };


    formSubmit() {
      var financeTypeValue=document.querySelector('input[name=radio-name-type-of-loan]:checked').value
      var homeTypeValue=document.querySelector('input[name=radio-name-home-description]:checked').value
      var propertyTypeValue=document.querySelector('input[name=radio-name-property-use]:checked').value
      var firstNameValue=document.getElementById('firstName').value
      var lastNameValue=document.getElementById('lastName').value
      var usernameValue=document.getElementById('username').value
      var passwordValue=document.getElementById('password').value
      
        const formDataJson={
          financeType:financeTypeValue,
          homeType:homeTypeValue,
          propertyType:propertyTypeValue,
          firstName:firstNameValue,
          lastName:lastNameValue,
          username:usernameValue,
          password:passwordValue
        }
        
      axios.post("http://35.232.52.140:8080/application",formDataJson,{headers:{"organization":"Lender"}} )
      .then(response => {
        if (response.data != null) {
          alert("Successfull")
          this.setState({ responseData: response.data });
          alert(JSON.stringify(this.state.responseData))
          window.location.href="http://localhost:3000/login"
       
        }
        
     })
    .catch(error => {
      //var this.state.errorResponse={firstName:[],lastName:[],username:[],password:[]};
      this.setState({ errorResponse: error.response.data });;
        alert(JSON.stringify(this.state.errorResponse.password)) 
        document.getElementById("error").innerHTML=JSON.stringify(this.state.errorResponse);
       // window.location.href="http://localhost:3000/register"
      });
    }


 fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}
  showTab() {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  
  x[this.state.currentTab].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (this.state.currentTab == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (this.state.currentTab== (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  this.fixStepIndicator(this.state.currentTab)
}

 nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !this.validateForm()) return false;
  // Hide the current tab:
  x[this.state.currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  this.state.currentTab = this.state.currentTab + n;
  // if you have reached the end of the form...
  if (this.state.currentTab >= x.length) {
    // ... the form gets submitted:
    //document.getElementById("regForm").submit();
    this.formSubmit();
    return false;
  }
  // Otherwise, display the correct tab:
  this.showTab();
}
 validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[this.state.currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[this.state.currentTab].className += " finish";
  }
  return valid; // return the valid status
}



render(){
  const errorResponse=this.state.errorResponse;
  const PasswordErrorList = () => (
    <ul>
      {errorResponse.password.map(function(item) {
        return <li key={item}>{item}</li>;
      })}
    </ul>
  );
    return (
      <div>
       
      <form id="regForm" style={{ color: "white" }} >
  {/* One "tab" for each step in the form: */}
  {/* Type of Loan */}
  <div className="tab image-radio" style={{ display: "block" }}>
    <h3 className="title-header">Choose type of loan</h3>
    <input
      type="radio"
      name="radio-name-type-of-loan"
      id="radio-id-home-purchase"
      className="radio-name-type-of-loan input-hidden"
      value="HOME_PURCHASE"
    />
    <label className="radio-inline" htmlFor="radio-id-home-purchase">
      <svg
        className="c-Icon--currentColor t-Wham-c-RadioInputWithIcon__svg"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 450.87 450.87"
      >
        <title>
          Illustration of a dollar sign surrounded by a semi-circle with an
          arrow at one end.
        </title>
        <g id="wham-icon--refi">
          <path d="M233.14,368.09A142.66,142.66,0,0,1,132.27,124.56c55.62-55.62,146.13-55.62,201.75,0L328.54,130c-52.6-52.6-138.19-52.6-190.79,0A134.91,134.91,0,0,0,328.54,320.83l5.48,5.48A141.75,141.75,0,0,1,233.14,368.09Z" />
          <polygon points="356.5 151.64 301.43 151.64 356.5 96.56 356.5 151.64" />
          <path d="M233.57,285.79v16.42H222V285.79a45.73,45.73,0,0,1-31.39-17.88L201,259.77a31.84,31.84,0,0,0,21.07,14.68v-41c-13.51-4.36-27.47-10.17-27.47-28.48,0-14.54,11.49-25.29,27.33-27.32v-15h11.62v15c11.77,1.3,19.91,6.68,26.45,14.53l-9.88,7.41a26,26,0,0,0-16.71-11.34V224.9c13.95,4.65,28.92,11,28.92,30.37C262.35,270.09,251.59,283.46,233.57,285.79Zm-11.48-64.67V188.27c-9.15,1.75-15.26,8-15.26,16.57C206.83,213.71,213.37,217.78,222.09,221.12Zm11.34,16v37.35c9.88-2,16.56-9.44,16.56-19C250,245.24,242.58,240.59,233.43,237.1Z" />
        </g>
      </svg>
      <span className="span-title">Home Purchase</span>
    </label>
    <input
      type="radio"
      name="radio-name-type-of-loan"
      id="radio-id-home-refinance"
      className="radio-name-type-of-loan input-hidden"
      value="HOME_REFINANCE"
    />
    <label className="radio-inline" htmlFor="radio-id-home-refinance">
      <svg
        className="c-Icon--currentColor t-Wham-c-RadioInputWithIcon__svg"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 450.87 450.87"
      >
        <title>Illustration of a house.</title>
        <g id="wham-icon--purch">
          <path d="M336.5,171.88v-73H275v15.82l-49.54-46L69.17,213.85l5.44,5.86,18.34-17v174h265v-174l18.33,17,5.45-5.86Zm-80.27,186.3v10h-72v-10Zm-56.42-8.28V279.31h40.87V349.9Zm150.11,18.76H264.23V350.18H248.68V271.31H191.81v78.87H176.27v18.48H101V195.25L225.43,79.66,283,133.09V106.92H328.5v68.45l21.42,19.89Z" />
        </g>
      </svg>
      <span className="span-title"> Home Refinance</span>
    </label>
    <input
      type="radio"
      name="radio-name-type-of-loan"
      id="radio-id-cash-out"
      className="radio-name-type-of-loan input-hidden"
      value="CASH_OUT_REFINANCE"
    />
    <label className="radio-inline" htmlFor="radio-id-cash-out">
      <svg
        className="c-Icon--currentColor t-Wham-c-RadioInputWithIcon__svg"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 450.87 450.87"
      >
        <title>Illustration of a house.</title>
        <g id="wham-icon--purch">
          <path d="M336.5,171.88v-73H275v15.82l-49.54-46L69.17,213.85l5.44,5.86,18.34-17v174h265v-174l18.33,17,5.45-5.86Zm-80.27,186.3v10h-72v-10Zm-56.42-8.28V279.31h40.87V349.9Zm150.11,18.76H264.23V350.18H248.68V271.31H191.81v78.87H176.27v18.48H101V195.25L225.43,79.66,283,133.09V106.92H328.5v68.45l21.42,19.89Z" />
        </g>
      </svg>
      <span className="span-title">Cash-Out Refinance</span>
    </label>
  </div>
  {/* Home Description */}
  <div className="tab image-radio">
    <h3>Home Description</h3>
    <input
      type="radio"
      name="radio-name-home-description"
      id="radio-id-single-family"
      className="radio-name-home-description input-hidden"
      value="SINGLE_FAMILY"
    />
    <label className="radio-inline " htmlFor="radio-id-single-family">
      <svg
        className="c-Icon--currentColor t-Wham-c-RadioInputWithIcon__svg"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 450.87 450.87"
      >
        <title>Illustration of a house.</title>
        <g id="wham-icon--purch">
          <path d="M336.5,171.88v-73H275v15.82l-49.54-46L69.17,213.85l5.44,5.86,18.34-17v174h265v-174l18.33,17,5.45-5.86Zm-80.27,186.3v10h-72v-10Zm-56.42-8.28V279.31h40.87V349.9Zm150.11,18.76H264.23V350.18H248.68V271.31H191.81v78.87H176.27v18.48H101V195.25L225.43,79.66,283,133.09V106.92H328.5v68.45l21.42,19.89Z" />
        </g>
      </svg>
      <span className="span-title">Single Family</span>
    </label>
    <input
      type="radio"
      name="radio-name-home-description"
      id="radio-id-multi-family"
      className="radio-name-home-description input-hidden"
      value="MULTI_FAMILY"
    />
    <label className="radio-inline" htmlFor="radio-id-multi-family">
      <svg
        className="c-Icon--currentColor t-Wham-c-RadioInputWithIcon__svg"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 450.87 450.87"
      >
        <title>Illustration of a multi-family home.</title>
        <g id="wham-icon--multi-fam">
          <path d="M284.28,83.78H156.6L93.13,147.25v222.9H347.75V147.25Zm-124.37,8H281L334.1,144.9H106.78Zm-4.61,252V277.44h38.43V343.8Zm143.46,8v8.94H142.12v-8.94Zm-53.57-8V277.44h38.43V343.8Zm61.57,18.35V343.82H291.62V269.44H237.19v74.38H201.73V269.44H147.3v74.38H134.12v18.33h-33V152.9H339.75V362.15Z" />
          <path d="M317.38,221.35v22.92H294.45V221.35h22.93m8-8H286.45v38.92h38.93V213.35Z" />
          <path d="M269.47,221.35v22.92H246.54V221.35h22.93m8-8Zm0,0H238.54v38.92h38.93V213.35Z" />
          <path d="M317.38,173.44v22.92H294.45V173.44h22.93m8-8H286.45v38.92h38.93V165.44Z" />
          <path d="M269.47,173.44v22.92H246.54V173.44h22.93m8-8Zm0,0H238.54v38.92h38.93V165.44Z" />
          <path d="M193,221.35v22.92H170.08V221.35H193m8-8H162.08v38.92H201V213.35Z" />
          <path d="M145.1,221.35v22.92H122.17V221.35H145.1m8-8Zm0,0H114.17v38.92H153.1V213.35Z" />
          <path d="M193,173.44v22.92H170.08V173.44H193m8-8H162.08v38.92H201V165.44Z" />
          <path d="M145.1,173.44v22.92H122.17V173.44H145.1m8-8Zm0,0H114.17v38.92H153.1V165.44Z" />
        </g>
      </svg>
      <span className="span-title">Multi Family</span>
    </label>
    <input
      type="radio"
      name="radio-name-home-description"
      id="radio-id-condominum"
      className="radio-name-home-description input-hidden"
      value="CONDOMINIUM"
    />
    <label className="radio-inline" htmlFor="radio-id-condominum">
      <svg
        className="c-Icon--currentColor t-Wham-c-RadioInputWithIcon__svg"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 450.87 450.87"
      >
        <title>Illustration of a condominium.</title>
        <g id="wham-icon--condo">
          <path d="M208.71,77.68v89.08H97.65v202H327.58V77.68ZM167,359.84v-28h21.18v28Zm41.55.9H196.14V323.8H159v36.94H105.65v-186H208.51Zm71.61-.9v-28H301.3v28Zm39.46.9H309.3V323.8H272.12v36.94H216.71V85.68H319.58Z" />
          <path d="M299.52,277.2v19.3h-19.3V277.2h19.3m8-8h-35.3v35.3h35.3V269.2Z" />
          <path d="M256.07,277.2v19.3h-19.3V277.2h19.3m8-8Zm0,0h-35.3v35.3h35.3V269.2Z" />
          <path d="M299.52,234.17v19.3h-19.3v-19.3h19.3m8-8h-35.3v35.3h35.3v-35.3Z" />
          <path d="M256.07,234.17v19.3h-19.3v-19.3h19.3m8-8Zm0,0h-35.3v35.3h35.3v-35.3Z" />
          <path d="M299.52,190.72V210h-19.3V190.72h19.3m8-8h-35.3V218h35.3V182.72Z" />
          <path d="M256.07,190.72V210h-19.3V190.72h19.3m8-8Zm0,0h-35.3V218h35.3V182.72Z" />
          <path d="M299.52,146.4v19.3h-19.3V146.4h19.3m8-8h-35.3v35.3h35.3V138.4Z" />
          <path d="M256.07,146.4v19.3h-19.3V146.4h19.3m8-8Zm0,0h-35.3v35.3h35.3V138.4Z" />
          <path d="M299.52,103v19.3h-19.3V103h19.3m8-8h-35.3v35.3h35.3V95Z" />
          <path d="M256.07,103v19.3h-19.3V103h19.3m8-8Zm0,0h-35.3v35.3h35.3V95Z" />
          <path d="M188.45,277.63v19.3h-19.3v-19.3h19.3m8-8h-35.3v35.3h35.3v-35.3Z" />
          <path d="M145,277.63v19.3h-19.3v-19.3H145m8-8Zm0,0h-35.3v35.3H153v-35.3Z" />
          <path d="M188.45,234.17v19.3h-19.3v-19.3h19.3m8-8h-35.3v35.3h35.3v-35.3Z" />
          <path d="M145,234.17v19.3h-19.3v-19.3H145m8-8Zm0,0h-35.3v35.3H153v-35.3Z" />
          <path d="M188.45,190.72V210h-19.3V190.72h19.3m8-8h-35.3V218h35.3V182.72Z" />
          <path d="M145,190.72V210h-19.3V190.72H145m8-8Zm0,0h-35.3V218H153V182.72Z" />
        </g>
      </svg>
      <span className="span-title">Condominum</span>
    </label>
    <input
      type="radio"
      name="radio-name-home-description"
      id="radio-id-townhouse"
      className="radio-name-home-description input-hidden"
      value="TOWNHOUSE"
    />
    <label className="radio-inline" htmlFor="radio-id-townhouse">
      <svg
        className="c-Icon--currentColor t-Wham-c-RadioInputWithIcon__svg"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 450.87 450.87"
      >
        <title>Illustration of a townhouse.</title>
        <g id="wham-icon--townhouse">
          <path d="M378.65,155.12,289.46,65.93l-65.13,65.13L159.19,65.93,70,155.12l5.66,5.65L90.23,146.2V371.67h268.3V146.31L373,160.77ZM195.23,362.2h-72v-10h72Zm-56.42-18.3V273.31h40.87V343.9Zm64.42,19.77V344.18H187.68V265.31H130.81v78.87H115.27v19.49h-17V138.2l61-61,61.08,61.07V363.67Zm122.26-1.47h-72v-10h72Zm-56.41-18.3V273.31H310V343.9Zm81.45,19.77h-17V344.18H318V265.31H261.08v78.87H245.53v19.49h-17V138.2l61-61,61.07,61.07Z" />
          <path d="M327.69,213.15v25.4h-25.4v-25.4h25.4m8-8h-41.4v41.4h41.4v-41.4Z" />
          <path d="M276.74,213.15v25.4H251.33v-25.4h25.41m8-8Zm0,0H243.33v41.4h41.41v-41.4Z" />
          <path d="M327.69,162.19v25.4h-25.4v-25.4h25.4m8-8h-41.4v41.4h41.4v-41.4Z" />
          <path d="M276.74,162.19v25.4H251.33v-25.4h25.41m8-8Zm0,0H243.33v41.4h41.41v-41.4Z" />
          <path d="M197.43,213.15v25.4H172v-25.4h25.4m8-8H164v41.4h41.4v-41.4Z" />
          <path d="M146.47,213.15v25.4h-25.4v-25.4h25.4m8-8Zm0,0h-41.4v41.4h41.4v-41.4Z" />
          <path d="M197.43,162.19v25.4H172v-25.4h25.4m8-8H164v41.4h41.4v-41.4Z" />
          <path d="M146.47,162.19v25.4h-25.4v-25.4h25.4m8-8Zm0,0h-41.4v41.4h41.4v-41.4Z" />
        </g>
      </svg>
      <span className="span-title">Townhouse</span>
    </label>
  </div>
  {/* Property Use */}
  <div className="tab image-radio">
    <h3 className="title-header">Property Use</h3>
    <input
      type="radio"
      name="radio-name-property-use"
      id="radio-id-primary-home"
      className="radio-name-property-use input-hidden"
      value="PRIMARY_HOME"
    />
    <label className="radio-inline" htmlFor="radio-id-primary-home">
      <svg
        className="c-Icon--currentColor t-Wham-c-RadioInputWithIcon__svg"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 450.87 450.87"
      >
        <title>Illustration of a mailbox.</title>
        <g id="wham-icon--primary">
          <path d="M227.7,379.71H172.29v-61.4H60.44V212a71.33,71.33,0,0,1,71.25-71.25h87.22v-64h79.92v50.11H226.91v13.91H296.6A71.33,71.33,0,0,1,367.84,212V318.31H227.7Zm-47.41-8H219.7v-61.4H359.84V212a63.31,63.31,0,0,0-63.24-63.25H131.69A63.32,63.32,0,0,0,68.44,212v98.33H180.29Zm46.63-252.89h63.92V84.71H226.92ZM180.55,294.21H82.83V213.69a48.86,48.86,0,0,1,97.72,0Zm-89.72-8h81.72V213.69a40.86,40.86,0,0,0-81.72,0Z" />
        </g>
      </svg>
      <span className="span-title">Primary<br/> Home</span>
    </label>
    <input
      type="radio"
      name="radio-name-property-use"
      id="radio-id-secondary-home"
      className="radio-name-property-use input-hidden"
      value="SECONDARY_HOME"
    />
    <label className="radio-inline" htmlFor="radio-id-secondary-home">
      <svg
        className="c-Icon--currentColor t-Wham-c-RadioInputWithIcon__svg"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 450.87 450.87"
      >
        <title>Illustration of a mailbox.</title>
        <g id="wham-icon--primary">
          <path d="M227.7,379.71H172.29v-61.4H60.44V212a71.33,71.33,0,0,1,71.25-71.25h87.22v-64h79.92v50.11H226.91v13.91H296.6A71.33,71.33,0,0,1,367.84,212V318.31H227.7Zm-47.41-8H219.7v-61.4H359.84V212a63.31,63.31,0,0,0-63.24-63.25H131.69A63.32,63.32,0,0,0,68.44,212v98.33H180.29Zm46.63-252.89h63.92V84.71H226.92ZM180.55,294.21H82.83V213.69a48.86,48.86,0,0,1,97.72,0Zm-89.72-8h81.72V213.69a40.86,40.86,0,0,0-81.72,0Z" />
        </g>
      </svg>
      <span className="span-title">Secondary Home</span>
    </label>
    <input
      type="radio"
      name="radio-name-property-use"
      id="radio-id-investment-property"
      className="radio-name-property-use input-hidden"
      value="INVESTMENT_PROPERTY"
    />
    <label className="radio-inline" htmlFor="radio-id-investment-property">
      <svg
        className="c-Icon--currentColor t-Wham-c-RadioInputWithIcon__svg"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 450.87 450.87"
      >
        <title>Illustration of a &amp;quot;For Rent&amp;quot; sign.</title>
        <g id="wham-icon--rental">
          <path d="M380.11,115.1H250.61v-36H196.25v36H66.76V296.45H196.25V381h54.36v-84.5h129.5ZM203.62,86.46h39.63V115.1H203.62Zm39.63,287.13H203.62V296.45h39.63Zm129.49-84.5H74.12V122.46H372.74Z" />
          <path d="M325.79,197.05h6.42v-7l6.36-4.14v11.16h9.06v5.82h-9.06v15.95c0,3.54,1.74,5.1,4.26,5.1a4.66,4.66,0,0,0,3.3-1.26l3.06,5.4a12,12,0,0,1-6.84,2c-6.42,0-10.14-3.18-10.14-11.15v-16h-6.42Z" />
          <path d="M292,197.05h6.36v4.5a12.34,12.34,0,0,1,10.32-5.16c8.22,0,13.32,5.4,13.32,14.27v18.78h-6.54V211.15c0-5.52-2.88-9.06-8.28-9.06-4.92,0-8.7,3.66-8.7,9.47v17.88H292Z" />
          <path d="M252.17,213.36a16.67,16.67,0,0,1,16.86-17c9.54,0,16.62,7,16.62,16.55v2.71H258.41c1,5.21,5,8.69,11,8.69a10.47,10.47,0,0,0,9.48-5.22l5.34,3.19a16.78,16.78,0,0,1-14.82,7.79C259.07,230.1,252.17,222.78,252.17,213.36Zm6.48-3.23h20.46c-1-5.1-4.74-8-10.08-8A10.58,10.58,0,0,0,258.65,210.13Z" />
          <path d="M215.21,185.65h17c9.54,0,15.72,6.18,15.72,14.64a13.9,13.9,0,0,1-8.46,13l9.3,16.2h-7.44L233.45,215H221.69v14.46h-6.48Zm6.48,6.06V209h10c6.66,0,9.72-4.25,9.72-8.87,0-5-3.3-8.4-9.72-8.4Z" />
          <path d="M174.35,197.05h6.36v5.64c1.38-4.08,4.68-6.12,8.82-6.12a10.8,10.8,0,0,1,5.46,1.26l-2.52,6.23a7.39,7.39,0,0,0-4.08-.95c-4.62,0-7.56,2.28-7.56,8.1v18.23h-6.48Z" />
          <path d="M134.45,213.24a16.86,16.86,0,1,1,16.86,16.86A16.8,16.8,0,0,1,134.45,213.24Zm27.3,0A10.42,10.42,0,1,0,151.31,224,10.57,10.57,0,0,0,161.75,213.24Z" />
          <path d="M101.39,185.65h27.18v6.24H107.93v12.48h20.64v6.24H107.93v18.83h-6.54Z" />
        </g>
      </svg>
      <span className="span-title">Investment Property</span>
    </label>
  </div>
  {/* When Do You Plan to Purchase Your Home*/}
  <div className="tab">
    <h3 className="title-header">When Do You Plan to Purchase Your Home?</h3>
    <ul className="list-group">
      <li className="list-group-item">
        {" "}
        <label className="form-check-label">
          <input
            type="radio"
            className="radio-name-plan-time form-check-input"
            name="radio-name-plan-time"
            value="immediately"
          />
          Immediately: Signed a Purchase Agreement
        </label>
      </li>
      <li className="list-group-item">
        {" "}
        <label className="form-check-label">
          <input
            type="radio"
            className="radio-name-plan-time form-check-input"
            name="radio-name-plan-time"
            value='asap'
          />
          ASAP: Found a House/Offer Pending
        </label>
      </li>
      <li className="list-group-item">
        {" "}
        <label className="form-check-label">
          <input
            type="radio"
            className="radio-name-plan-time form-check-input"
            name="radio-name-plan-time"
            value="within_30_days"
          />
          Within 30 Days
        </label>
      </li>
      <li className="list-group-item">
        {" "}
        <label className="form-check-label">
          <input
            type="radio"
            className="radio-name-plan-time form-check-input"
            name="radio-name-plan-time"
            value="2_3_months"
          />
          2 - 3 Months
        </label>
      </li>
      <li className="list-group-item">
        {" "}
        <label className="form-check-label">
          <input
            type="radio"
            className="radio-name-plan-time form-check-input"
            name="radio-name-plan-time"
            value="3_6_months"
          />
          3 - 6 Months
        </label>
      </li>
      <li className="list-group-item">
        {" "}
        <label className="form-check-label">
          <input
            type="radio"
            className="radio-name-plan-time form-check-input"
            name="radio-name-plan-time"
            value="6_months_or_more"
          />
          6+ Months
        </label>
      </li>
    </ul>
  </div>
 
  <div className="tab image-radio">
    <h3>Are you a first-time home buyer?</h3>
    <input
      type="radio"
      name="radio-name-first-time"
      id="radio-id-yes"
      className="radio-name-first-time input-hidden"
      value="yes"
    />
    <label className="radio-inline " htmlFor="radio-id-yes">
      <svg
        className="c-Icon--currentColor t-Wham-c-RadioInputWithIcon__svg"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 450.87 450.87"
      >
        <title>
          Illustration of a checkmark, signifying &amp;quot;yes.&amp;quot;
        </title>
        <g id="wham-icon--yes">
          <path d="M226.54,89.31a136.12,136.12,0,1,1-96.25,39.87,135.25,135.25,0,0,1,96.25-39.87m0-8a144.12,144.12,0,1,0,101.91,42.21A143.68,143.68,0,0,0,226.54,81.31Z" />
          <polygon points="196.47 306.08 141.29 250.91 146.95 245.25 195.63 293.93 305.78 148.44 312.16 153.27 196.47 306.08" />
        </g>
      </svg>
      <span className="span-title">Yes</span>
    </label>
    <input
      type="radio"
      name="radio-name-first-time"
      id="radio-id-no"
      className="radio-name-first-time input-hidden"
      value="no"
    />
    <label className="radio-inline" htmlFor="radio-id-no">
      <svg
        className="c-Icon--currentColor t-Wham-c-RadioInputWithIcon__svg"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 450.87 450.87"
      >
        <title>
          Illustration of a circle with a line drawn through it, signifying
          &amp;quot;no.&amp;quot;
        </title>
        <g id="wham-icon--no">
          <path d="M225.43,90.31a136.12,136.12,0,1,1-96.25,39.87,135.25,135.25,0,0,1,96.25-39.87m0-8a144.12,144.12,0,1,0,101.91,42.21A143.68,143.68,0,0,0,225.43,82.31Z" />
          <rect
            x="87.03"
            y="223.15"
            width="275.09"
            height={8}
            transform="translate(-92.16 268.44) rotate(-52.75)"
          />
        </g>
      </svg>
      <span className="span-title">No</span>
    </label>
  </div>
  <div className="tab image-radio">
    <h3>Credit Profile</h3>
    <input
      type="radio"
      name="radio-name-credit-profile"
      id="radio-id-excellent"
      className="radio-name-credit-profile input-hidden"
      value="excellent"
    />
    <label className="radio-inline " htmlFor="radio-id-excellent">
      <svg
        className="c-Icon--currentColor t-Wham-c-RadioInputWithIcon__svg"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 450.87 450.87"
      >
        <title>Illustration of an excited face.</title>
        <g id="wham-icon--excellent">
          <path d="M225.43,89.31a136.12,136.12,0,1,1-96.25,39.87,135.25,135.25,0,0,1,96.25-39.87m0-8a144.12,144.12,0,1,0,101.91,42.21A143.68,143.68,0,0,0,225.43,81.31Z" />
          <path d="M226,308.08c-44.41,0-89.81-7.1-89.81-59.8v-4H315.82v4C315.82,301,270.43,308.08,226,308.08Zm-81.71-55.8c.94,18.17,8.45,30.66,22.87,38.12,13,6.7,31.1,9.68,58.84,9.68s45.88-3,58.83-9.68c14.42-7.46,21.93-19.95,22.87-38.12Z" />
          <path d="M190.22,211.74A16.75,16.75,0,1,1,207,195,16.76,16.76,0,0,1,190.22,211.74Zm0-25.5A8.75,8.75,0,1,0,199,195,8.76,8.76,0,0,0,190.22,186.24Z" />
          <path d="M261.18,211.75A16.75,16.75,0,1,1,270.46,181h0a16.76,16.76,0,0,1-9.28,30.72Zm.05-25.5A8.74,8.74,0,1,0,266,187.7h0A8.68,8.68,0,0,0,261.23,186.25Z" />
        </g>
      </svg>
      <span className="span-title">Excellent( 720+ )</span>
    </label>
    <input
      type="radio"
      name="radio-name-credit-profile"
      id="radio-id-good"
      className="radio-name-credit-profile input-hidden"
      value="good"
    />
    <label className="radio-inline" htmlFor="radio-id-good">
      <svg
        className="c-Icon--currentColor t-Wham-c-RadioInputWithIcon__svg"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 450.87 450.87"
      >
        <title>Illustration of a smiling face.</title>
        <g id="wham-icon--good">
          <path d="M225.43,89.31a136.12,136.12,0,1,1-96.25,39.87,135.25,135.25,0,0,1,96.25-39.87m0-8a144.12,144.12,0,1,0,101.91,42.21A143.68,143.68,0,0,0,225.43,81.31Z" />
          <path d="M225.43,308.08c-44.4,0-89.8-7.1-89.8-59.8h8c0,20.35,7.51,34.13,23,42.12,13,6.7,31.1,9.68,58.83,9.68s45.88-3,58.84-9.68c15.46-8,23-21.77,23-42.12h8C315.24,301,269.84,308.08,225.43,308.08Z" />
          <path d="M190.21,211.74A16.75,16.75,0,1,1,207,195,16.76,16.76,0,0,1,190.21,211.74Zm0-25.5A8.75,8.75,0,1,0,199,195,8.76,8.76,0,0,0,190.21,186.24Z" />
          <path d="M261.19,211.74a16.75,16.75,0,1,1,14-7.5A16.66,16.66,0,0,1,261.19,211.74Zm0-25.5a8.84,8.84,0,0,0-1.76.18A8.75,8.75,0,1,0,266,187.7h0A8.68,8.68,0,0,0,261.23,186.24Z" />
        </g>
      </svg>
      <span className="span-title">Good( 660-719 )</span>
    </label>
    <input
      type="radio"
      name="radio-name-credit-profile"
      id="radio-id-avg"
      className="radio-name-credit-profile input-hidden"
      value="average"
    />
    <label className="radio-inline" htmlFor="radio-id-avg">
      <svg
        className="c-Icon--currentColor t-Wham-c-RadioInputWithIcon__svg"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 450.87 450.87"
      >
        <title>Illustration of a smirking face.</title>
        <g id="wham-icon--average">
          <path d="M225.43,89.31a136.12,136.12,0,1,1-96.25,39.87,135.25,135.25,0,0,1,96.25-39.87m0-8a144.12,144.12,0,1,0,101.91,42.21A143.68,143.68,0,0,0,225.43,81.31Z" />
          <path d="M214.43,308.11c-28.13,0-54.14-1.44-68.06-27.2l7-3.8c12.52,23.16,36.26,23.1,66.31,23,3.53,0,7.14,0,10.68,0,30.6.08,54.79.13,67.34-23.51l7.06,3.75C290,308.23,261,308.16,230.38,308.09c-3.53,0-7.12,0-10.63,0Z" />
          <path d="M190.22,211.74A16.75,16.75,0,1,1,207,195,16.76,16.76,0,0,1,190.22,211.74Zm0-25.5A8.75,8.75,0,1,0,199,195,8.76,8.76,0,0,0,190.22,186.24Z" />
          <path d="M261.18,211.75a16.77,16.77,0,1,1,3.38-.34A16.92,16.92,0,0,1,261.18,211.75Zm.05-25.5A8.74,8.74,0,1,0,266,187.7,8.74,8.74,0,0,0,261.23,186.25Z" />
        </g>
      </svg>
      <span className="span-title">Average( 620-659 )</span>
    </label>
    <input
      type="radio"
      name="radio-name-credit-profile"
      id="radio-id-below-avg"
      className="radio-name-credit-profile input-hidden"
      value="below_average"
    />
    <label className="radio-inline" htmlFor="radio-id-below-avg">
      <svg
        className="c-Icon--currentColor t-Wham-c-RadioInputWithIcon__svg"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 450.87 450.87"
      >
        <title>Illustration of an expressionless face.</title>
        <g id="wham-icon--below-average">
          <path d="M225.43,89.31a136.12,136.12,0,1,1-96.25,39.87,135.25,135.25,0,0,1,96.25-39.87m0-8a144.12,144.12,0,1,0,101.91,42.21A143.68,143.68,0,0,0,225.43,81.31Z" />
          <rect x="164.67" y="289.54" width="124.67" height={8} />
          <path d="M190.21,211.74A16.75,16.75,0,1,1,207,195,16.76,16.76,0,0,1,190.21,211.74Zm0-25.5A8.75,8.75,0,1,0,199,195,8.76,8.76,0,0,0,190.21,186.24Z" />
          <path d="M261.18,211.75a16.77,16.77,0,1,1,3.38-.34A16.92,16.92,0,0,1,261.18,211.75Zm.05-25.51a8.84,8.84,0,0,0-1.76.18A8.75,8.75,0,1,0,266,187.7h0A8.68,8.68,0,0,0,261.23,186.24Z" />
        </g>
      </svg>
      <span className="span-title">Below Avg( 580-619 )</span>
    </label>
    <input
      type="radio"
      name="radio-name-credit-profile"
      id="radio-id-poor"
      className="radio-name-credit-profile input-hidden"
      value="poor"
    />
    <label className="radio-inline" htmlFor="radio-id-poor">
      <svg
        className="c-Icon--currentColor t-Wham-c-RadioInputWithIcon__svg"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 450.87 450.87"
      >
        <title>Illustration of a sad face.</title>
        <g id="wham-icon--poor">
          <path
            id="circletestbreh"
            d="M225.43,89.31a136.12,136.12,0,1,1-96.25,39.87,135.25,135.25,0,0,1,96.25-39.87m0-8a144.12,144.12,0,1,0,101.91,42.21A143.68,143.68,0,0,0,225.43,81.31Z"
          />
          <path d="M156.26,306l-7.07-3.75c14.82-27.9,43.78-27.84,74.43-27.76h10.63c30.14-.1,58.58-.19,73.37,27.18l-7,3.8c-12.51-23.16-36.26-23.09-66.3-23H223.6C193,282.4,168.81,282.34,156.26,306Z" />
          <path d="M190.22,211.74A16.75,16.75,0,1,1,207,195,16.76,16.76,0,0,1,190.22,211.74Zm0-25.5A8.75,8.75,0,1,0,199,195,8.76,8.76,0,0,0,190.22,186.24Z" />
          <path d="M261.18,211.75a16.77,16.77,0,1,1,3.38-.34A16.92,16.92,0,0,1,261.18,211.75Zm.05-25.5A8.74,8.74,0,1,0,266,187.7,8.74,8.74,0,0,0,261.23,186.25Z" />
        </g>
      </svg>
      <span className="span-title">Poor( less than 579 )</span>
    </label>
  </div>
  <div className="tab">
    <div>
      <span className="span-title" style={{ float: "left" }}>
        First Name
      </span>
      <input
        id="firstName"
        type="text"
        placeholder="Enter FirstName"
        name="firstName"
      />
    </div>
    <div>
      <span className="span-title" style={{ float: "left" }}>
        Last Name
      </span>
      <input
        id="lastName"
        type="text"
        placeholder="Enter LastName"
        name="lastName"
      />
    </div>
  </div>
  <div className="tab">
    <div>
      <span className="span-title" style={{ float: "left" }}>
        Username
      </span>
      <input
        id="username"
        type="text"
        placeholder="Enter Email"
        name="username"
      />
    </div>
  </div>
  <div className="tab">
    <div>
      <span style={{ float: "left" }} className="span-title">
        Password :
      </span>
      <input
      id="password"
        type="password"
        placeholder="Enter Password"
        name="password"
      />
    </div>
    <div>
      <span className="span-title" style={{ float: "left" }}>
        Confirm Password :
      </span>
      <input
       id="confirmPassword"
        type="password"
        placeholder="Confirm Password"
        name="confirmPassword"
      />
    </div>
  </div>
  <div style={{ overflow: "auto", marginTop: 15 }}>
    <div style={{ float: "right" }}>
      <button type="button"style={{background:'#de063c',display:'none',width:120}} id="prevBtn"
      onClick={()=>{this.nextPrev(-1)}}>
        Previous
      </button>
      <button type="button" style={{background:'#de063c',marginLeft:20,width:120}} id="nextBtn"onClick={()=>{this.nextPrev(1)}} >
        Next
      </button>
    </div>
  </div>
  {/* Circles which indicates the steps of the form: */}
  <div style={{ textAlign: "center", marginTop: 40 }}>
    <span className="step" />
    <span className="step" />
    <span className="step" />
    <span className="step" />
    <span className="step" />
    <span className="step" />
    <span className="step" />
    <span className="step" />
    <span className="step" />
  </div>
</form>
<p id="error" style={{textAlign:"left",color:"red"}}></p>
<div>
</div>
<PasswordErrorList/>
</div>
      );
}
}