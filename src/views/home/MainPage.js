import React from 'react';
import HomeBanner from './HomeBanner';
import HomeServices from './HomeServices.js';
import Pricing from './Pricing';
import Footer from './Footer';
import Navigation from './Navigation';



export default class MainPage extends React.Component {
render(){
	return (
    <div className="header">
      <Navigation/>
      <HomeBanner/>
      <HomeServices/>
      <Pricing/>
      <Footer/>
    </div>
    );
}
}