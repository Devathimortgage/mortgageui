import React from 'react';


export default class Pricing extends React.Component {
render(){
    return (
<section className="price_area p_120">
  <div className="container">
    <div className="main_title">
      <h2>Choose Your Price Plan</h2>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
        veniam, quis nostrud exercitation.
      </p>
    </div>
    <div className="price_inner row">
      <div className="col-lg-4">
        <div className="price_item">
          <div className="price_head">
            <h4>Real Basic</h4>
          </div>
          <div className="price_body">
            <ul className="list">
              <li>
                <a href="#">2.5 GB Space</a>
              </li>
              <li>
                <a href="#">Secure Online Transfer</a>
              </li>
              <li>
                <a href="#">Unlimited Styles</a>
              </li>
              <li>
                <a href="#">Customer Service</a>
              </li>
            </ul>
          </div>
          <div className="price_footer">
            <h3>
              <span className="dlr">$</span> 39{" "}
              <span className="month">
                Per <br />
                Month
              </span>
            </h3>
            <a className="main_btn" href="#">
              Get Started
            </a>
          </div>
        </div>
      </div>
      <div className="col-lg-4">
        <div className="price_item">
          <div className="price_head">
            <h4>Real Standard</h4>
          </div>
          <div className="price_body">
            <ul className="list">
              <li>
                <a href="#">10 GB Space</a>
              </li>
              <li>
                <a href="#">Secure Online Transfer</a>
              </li>
              <li>
                <a href="#">Unlimited Styles</a>
              </li>
              <li>
                <a href="#">Customer Service</a>
              </li>
            </ul>
          </div>
          <div className="price_footer">
            <h3>
              <span className="dlr">$</span> 69{" "}
              <span className="month">
                Per <br />
                Month
              </span>
            </h3>
            <a className="main_btn" href="#">
              Get Started
            </a>
          </div>
        </div>
      </div>
      <div className="col-lg-4">
        <div className="price_item">
          <div className="price_head">
            <h4>Real Ultimate</h4>
          </div>
          <div className="price_body">
            <ul className="list">
              <li>
                <a href="#">Unlimited Space</a>
              </li>
              <li>
                <a href="#">Secure Online Transfer</a>
              </li>
              <li>
                <a href="#">Unlimited Styles</a>
              </li>
              <li>
                <a href="#">Customer Service</a>
              </li>
            </ul>
          </div>
          <div className="price_footer">
            <h3>
              <span className="dlr">$</span> 99{" "}
              <span className="month">
                Per <br />
                Month
              </span>
            </h3>
            <a className="main_btn" href="#">
              Get Started
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

    );
}
}