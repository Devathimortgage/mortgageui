import React from 'react';
import IdleTimer from 'react-idle-timer';
import {Redirect} from 'react-router-dom';

export default class Navigation extends React.Component {
  constructor(props)
  {
    super(props);
    this.state = {
      userName:"",
      isLogged:false,
      isTimeOut:false,
      time:30000
    }
  }
  componentDidMount(){
      let userName = sessionStorage.getItem('userName');
      if(userName !==null)
      {
          userName = userName.split("@")[0];
          this.setState({userName:userName});
          this.setState({isLogged:true});
          let maxTime = sessionStorage.getItem('sessionMaxTime');
          this.setState({time:30000});
      }
  }
   handleOnAction = (event) =>{
    console.log('user did something', event)
  }
 
   handleOnActive = (event)=> {
    console.log('user is active', event)
    // console.log('time remaining', this.idleTimer.getRemainingTime())
  }
 
   handleOnIdle = (event)=> {
    console.log('user is idle', event)
    this.setState({isTimeOut:true});
    window.alert("Session expired. Please Login Again")
   
    sessionStorage.clear();
    // console.log('last active', this.idleTimer.getLastActiveTime())
  }
 

render(){
    return (
        <header className="header_area">
          {this.state.isLogged?<IdleTimer
            // ref={ref => { this.idleTimer = ref }}
            element={document}
            onActive={this.handleOnActive}
            onIdle={this.handleOnIdle}
            onAction={this.handleOnAction}
            debounce={250}
            timeout={this.state.time} />:null}
        {this.state.isTimeOut?  <Redirect to="/login"/>:null}
        <div className="main_menu">
          <nav className="navbar navbar-expand-lg navbar-light">
            <div className="container box_1620">
              {/* Brand and toggle get grouped for better mobile display */}
              <a className="navbar-brand logo_h" href="/home" style={{color:'#ffffff'}}>
                {/* <img src="img/logo.png"  /> */}
                Mortgage
              </a>
              <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="icon-bar" />
                <span className="icon-bar" />
                <span className="icon-bar" />
              </button>
              {/* Collect the nav links, forms, and other content for toggling */}
              <div
                className="collapse navbar-collapse offset"
                id="navbarSupportedContent"
              >
                <ul className="nav navbar-nav menu_nav justify-content-center">
                  <li className="nav-item">
                    <a className="nav-link" href="/home">
                    Home
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#services">
                      Services
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href={'/'}>
                      Pricing
                    </a>
                  </li>
                  
                  <li className="nav-item">
                    <a className="nav-link" href="contact.html">
                      Contact
                    </a>
                  </li>
                  
                </ul>
                <ul className="nav navbar-nav navbar-right menu_nav justify-content-center ">
                  <li className="nav-item">
                    <a href="/form" className="tickets_btn">
                      Get Started
                    </a>
                  </li>
                  {this.state.userName.length>0?
                  <li className="nav-item">
                  <a className="navbar-brand logo_h" href="/home" style={{color:'#ffffff'}}>
                     Welcome {this.state.userName}
                    </a>
                  <a className="tickets_btn" onClick={()=>{sessionStorage.clear()}}  href="/login">
                    Sign Out
                  </a>
                </li>:
                  <li className="nav-item">
                    <a className="tickets_btn" href="/login">
                      Sign In
                    </a>
                  </li>
}
                </ul>
              </div>
            </div>
          </nav>
        </div>
      </header> 
  
    );
}
}