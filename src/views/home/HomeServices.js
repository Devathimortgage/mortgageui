import React from 'react';


export default class HomeServices extends React.Component {
render(){
    return (
          <section id="services" className="work_area p_120">
            <div className="container">
              <div className="main_title">
                <h2>Our Services</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                  veniam, quis nostrud exercitation.
                </p>
              </div>
              <div className="work_inner row">
                <div className="col-lg-4">
                  <div className="work_item">
                    <i className="lnr lnr-screen" />
                    <a href="#">
                      <h4>Stunning Visuals</h4>
                    </a>
                    <p>
                      Here, I focus on a range of items and features that we use in life
                      without giving them a second thought such as Coca Cola.
                    </p>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="work_item">
                    <i className="lnr lnr-code" />
                    <a href="#">
                      <h4>Stunning Visuals</h4>
                    </a>
                    <p>
                      Here, I focus on a range of items and features that we use in life
                      without giving them a second thought such as Coca Cola.
                    </p>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="work_item">
                    <i className="lnr lnr-clock" />
                    <a href="#">
                      <h4>Stunning Visuals</h4>
                    </a>
                    <p>
                      Here, I focus on a range of items and features that we use in life
                      without giving them a second thought such as Coca Cola.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </section>
        
    );
}
}