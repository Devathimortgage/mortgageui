import React from 'react';
import FormDetails from './FormDetails';
import { Route, Link, BrowserRouter } from 'react-router-dom'
export default class HomeBanner extends React.Component {
render(){
	return (
    
          <section className="home_banner_area" style={{background:'rebeccapurple'}}>
            <div className="banner_inner">
              <div className="container">
                <div className="row">
                <div className="col-lg-5">
                    <div className="banner_content">
                      <h2>
                        Mortgage Easy <br />
                      </h2>
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                        enim ad minim. sed do eiusmod tempor incididunt.
                      </p>
                      <a className="banner_btn" href="/form">
                       Home Purchase
                      </a>
                      <a className="banner_btn2" href="/form">
                        Home Refinance
                      </a>
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="home_left_img">
                      <img className="img-fluid" style= {{marginLeft: 75,marginTop: -20}}src="img/home-img.png" />
                    </div>
                  </div> 
                </div>
              </div>
            </div>
          </section>
        
        );
}
}