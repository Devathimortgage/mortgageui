import React from 'react'
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

const Customer = () => {
  return (
    <>
    
     <h3>Mortgage Dashboard</h3>
     <hr/>
     <br/>
     <h5>Loans You're Applying For</h5>
    <CRow>
        <CCol>
          <CCard>
          <CCardBody>
             <CRow>
               <CCol>
               <h6>Purchase Application</h6>
               <p>Started on July 16, 2020</p>
               </CCol>
               <CCol>
               <CButton block color="primary">Continue Application</CButton>
               </CCol>
               <CCol>
               <CButton block color="secondary">Delete My Application</CButton>
               </CCol>
             </CRow>
            </CCardBody>
          </CCard>
         </CCol>
         
      </CRow>

      <br/>
     <h5>Loans You're Paying For</h5>
    <CRow>
        <CCol>
          <CCard>
          <CCardBody>
             <CRow>
               <CCol>
               <h6>2096 Lake View</h6>
               <p>Purchase-Loan #342568975</p>
               </CCol>
               <CCol>
               <CButton block color="primary">View My Application</CButton>
               </CCol>
               <CCol>
               <CButton block color="secondary">Refinance Application</CButton>
               </CCol>
             </CRow>
            </CCardBody>
          </CCard>
         </CCol>
         
      </CRow>
   
    </>
  )
}

export default Customer
 