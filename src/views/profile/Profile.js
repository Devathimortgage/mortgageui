import React from 'react';
import {
    CBadge,
    CButton,
    CButtonGroup,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CProgress,
    CRow,
    CDataTable
  } from '@coreui/react'
  import {
    CImg
  } from '@coreui/react'
  import {
    TheContent,
    TheSidebar,
    TheFooter,
    TheHeader
  } from '../../containers/index'
  
import CIcon from '@coreui/icons-react'
  
export default class HomeServices extends React.Component {
render(){
    return (
        <div className="c-app c-default-layout">
      <TheSidebar/>
      <div className="c-wrapper">
        <TheHeader/>
        <div className="c-body" style={{margin:10}}>

        <div className="container-fluid">
          <div className="row">
    {/* Column */}
    <div className="col-lg-4 col-xlg-3 col-md-5">
      <div className="card">
        <h5 style={{margin:10,textAlign:'center'}}>My Profile</h5>
        <div className="card-body">
          <center className="m-t-30">
            
            <CImg
            src={'https://dummyimage.com/150x150.jpg/5fa2dd/ffffff'}
            className="c-avatar-img"
            alt="admin@bootstrapmaster.com"
            style={{width:150,height:150}}
          />
            <h4 className="card-title m-t-10">Hanna Gover</h4>
            <h6 className="card-subtitle">Mortgage Agent</h6>
          </center>
        </div>
        <div>
          <hr />
        </div>
        <div className="card-body">
          
          <small className="text-muted">Email address </small>
          <h6>hannagover@gmail.com</h6>
          <small className="text-muted p-t-30 db">Phone</small>
          <h6>+91 654 784 547</h6>
          <small className="text-muted p-t-30 db">Address</small>
          <h6>71 Pilgrim Avenue Chevy Chase, MD 20815</h6>
         {/*  <div className="map-box">
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d470029.1604841957!2d72.29955005258641!3d23.019996818380896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e848aba5bd449%3A0x4fcedd11614f6516!2sAhmedabad%2C+Gujarat!5e0!3m2!1sen!2sin!4v1493204785508"
              width="100%"
              height={150}
              frameBorder={0}
              style={{ border: 0 }}
              allowFullScreen
            />
          </div> */}
          <small className="text-muted p-t-30 db">Social Profile</small>
          <br />
          <center >
            <CIcon style={{margin:10}} name='cib-facebook'></CIcon>
            <CIcon style={{margin:10}} name='cib-instagram'></CIcon>
            <CIcon style={{margin:10}} name='cib-twitter'></CIcon>
          </center>
         
        </div>
      </div>
    </div>
    {/* Column */}
    {/* Column */}
    <div className="col-lg-8 col-xlg-9 col-md-7">
     <div className="card">
      <h5 style={{margin:10,textAlign:'center'}}>Update Profile</h5>
        <div className="card-body">
          <form className="form-horizontal form-material">
            <div className="form-group">
              <label className="col-md-12">Full Name</label>
              <div className="col-md-12">
                <input
                  type="text"
                  placeholder="Johnathan Doe"
                  className="form-control form-control-line"
                />
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="example-email" className="col-md-12">
                Email
              </label>
              <div className="col-md-12">
                <input
                  type="email"
                  placeholder="johnathan@admin.com"
                  className="form-control form-control-line"
                  name="example-email"
                  id="example-email"
                />
              </div>
            </div>
            <div className="form-group">
              <label className="col-md-12">Password</label>
              <div className="col-md-12">
                <input
                  type="password"
                  defaultValue="password"
                  className="form-control form-control-line"
                />
              </div>
            </div>
            <div className="form-group">
              <label className="col-md-12">Phone No</label>
              <div className="col-md-12">
                <input
                  type="text"
                  placeholder="123 456 7890"
                  className="form-control form-control-line"
                />
              </div>
            </div>
            <div className="form-group">
              <label className="col-md-12">Message</label>
              <div className="col-md-12">
                <textarea
                  rows={5}
                  className="form-control form-control-line"
                  defaultValue={""}
                />
              </div>
            </div>
            <div className="form-group">
              <label className="col-sm-12">Select Country</label>
              <div className="col-sm-12">
                <select className="form-control form-control-line">
                  <option>United Kingdom</option>
                  <option>India</option>
                  <option>Usa</option>
                  <option>Canada</option>
                  <option>Thailand</option>
                </select>
              </div>
            </div>
            <div className="form-group">
              <div className="col-sm-12">
                <button className="btn btn-primary" style={{float:"right"}}>Update Profile</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    {/* Column */}
  </div>
</div>

       
              </div>
        <TheFooter/>
      </div>
    </div>
      
    );
    }
}
