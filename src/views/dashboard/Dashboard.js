import React from 'react'
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CDataTable
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import usersData from '../users/UsersData'
import WidgetsDropdown from '../widgets/WidgetsDropdown'
import axios from "axios";

const getBadge = status => {
  switch (status) {
    case 'Active': return 'success'
    case 'Inactive': return 'secondary'
    case 'Pending': return 'warning'
    case 'Banned': return 'danger'
    default: return 'primary'
  }
}
const fields = ['profile','userName','email','action']
export default class HomeServices extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tableData:[{id: '', profile: '', userName: '', email: ''}],
    }
}
  componentDidMount(){
    axios.get("https://my.api.mockaroo.com/usersdata.json?key=8239c9d0")
    .then(response => {
        if (response.data != null) {
          this.setState({ tableData: response.data });
        }
    })
  }
  render(){
    const { tableData } = this.state;
    return (
      <>
      <WidgetsDropdown/>
        <CRow>
          <CCol>
            <CCard>
            <CCardHeader>
                <h5>Pending Approvals</h5>
              </CCardHeader>
              <CCardBody>
              <CDataTable
                items={tableData}
                fields={fields}
                striped
                tableFilter
                itemsPerPageSelect
                itemsPerPage={5}
                pagination={{align:"end"}}
                scopedSlots = {{     
              'action':
            (item, index)=>{
              return (
                <td>
                <CButtonGroup>
                  <CButton color="primary">Approve</CButton>
                  <CButton color="secondary">Reject</CButton>
                </CButtonGroup>
                </td> )
            },
            'profile':
            (item, index)=>{
              return (
                <td className="text-center">
            <div className="c-avatar">
              <img src={`${item.profile}.png`} className="c-avatar-img" />
              <span className="c-avatar-status bg-success"></span>
            </div>
          </td>
   )
            },
           
                }}
              />
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </>
    );
  }
  }