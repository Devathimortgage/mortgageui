describe('Home Purchase Button Click', () => {
    it('Type of loan - Home Purchase', () => {
      
      cy.visit('http://localhost:3000/')
      cy.contains('Home Purchase').click()
      
        cy.visit('http://localhost:3000/form')

        cy.get('#radio-id-home-purchase').check({ force: true },{ multiple: true })
        cy.get('#nextBtn').click()

        cy.get('.radio-name-home-description').check({ force: true },{ multiple: true })
        cy.get('#nextBtn').click()
        
        cy.get('.radio-name-property-use').check({ force: true },{ multiple: true })
        cy.get('#nextBtn').click()

        cy.get('.form-check-input').check({ force: true },{ multiple: true })
        cy.get('#nextBtn').click()

        cy.get('.radio-name-first-time').check({ force: true },{ multiple: true })
        cy.get('#nextBtn').click()
        
        cy.get('.radio-name-credit-profile').check({ force: true },{ multiple: true })
        cy.get('#nextBtn').click()

        cy.get('#firstName').type('fakeFirstName').should('have.value', 'fakeFirstName')

         cy.get('#lastName').type('fakeLastName').should('have.value', 'fakeLastName')

        cy.get('#nextBtn').click()

        cy.get('#password').type('fake@123').should('have.value', 'fake@123')

        cy.get('#confirmPassword').type('fake@123').should('have.value', 'fake@123')

      
    })
  })
describe('Home Refinance Button Click', () => {
    it('Type of loan - Home Refinance', () => {
      
      cy.visit('http://localhost:3000/')
      cy.contains('Home Refinance').click()
      
        cy.visit('http://localhost:3000/form')

        cy.get('#radio-id-home-refinance').check({ force: true },{ multiple: true })
        cy.get('#nextBtn').click()

        cy.get('.radio-name-home-description').check({ force: true },{ multiple: true })
        cy.get('#nextBtn').click()
        
        cy.get('.radio-name-property-use').check({ force: true },{ multiple: true })
        cy.get('#nextBtn').click()

        cy.get('.form-check-input').check({ force: true },{ multiple: true })
        cy.get('#nextBtn').click()

        cy.get('.radio-name-first-time').check({ force: true },{ multiple: true })
        cy.get('#nextBtn').click()
        
        cy.get('.radio-name-credit-profile').check({ force: true },{ multiple: true })
        cy.get('#nextBtn').click()

        cy.get('#firstName').type('fakeFirstName').should('have.value', 'fakeFirstName')

        cy.get('#lastName').type('fakeLastName').should('have.value', 'fakeLastName')

        cy.get('#nextBtn').click()

        cy.get('#password').type('fake@123').should('have.value', 'fake@123')

        cy.get('#confirmPassword').type('fake@123').should('have.value', 'fake@123')

    })
  })